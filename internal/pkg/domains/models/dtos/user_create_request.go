package dtos

type UserCreateRequest struct {
	Username string `form:"username" binding:"required,alphaNumeric"`
	Email    string `form:"email" binding:"required"`
	Password string `form:"password" binding:"required"`
}
